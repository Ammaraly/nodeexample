const fs = require('fs');
fs.writeFile('example.js', "console.log(`This is an example`);", (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("File successfully created");
        fs.readFile('example.js', 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log(data);
                const readLine = require('readline');
                const rl = readLine.createInterface({ input: process.stdin, output: process.stdout });
                rl.question(`Would you like to rename the file? \n`,
                    (userInput) => {
                        const yes = ["y", "yes"];
                        if (userInput.toLowerCase().trim() in yes) {
                            rl.setPrompt("Enter file name:\n");
                            rl.prompt();
                            rl.on('line', (fileName) => {
                                fs.rename('example.js', fileName.trim());
                            });
                        } else {
                            rl.close();
                        }
                    });
            }
        });

    }
});