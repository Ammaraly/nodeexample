const template = require('./template');
var sample = "sadsad{{data1}}asdsadsadda{{data2}}asdasdsad{{}}";
console.log(template.getPlaceHolders(sample));

const EventEmitter = require('events');
eventEmitter = new EventEmitter;

eventEmitter.on('message', (num1, num2) => {
    console.log('message : ' + (num1 + num2));
});

eventEmitter.emit('message',1,2)

class Person extends EventEmitter {
    constructor(name) {
        super();
        this._name = name;
        this.on('name',  ()=> {
            console.log("My name is " + this._name);
        });
    }

    get name() {
        return this._name;
    }
}

pedro = new Person("Pedro");
pedro.emit('name');